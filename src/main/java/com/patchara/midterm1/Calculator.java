/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.midterm1;

import java.util.Scanner;

/**
 *
 * @author user1
 */
public class Calculator {

    Scanner kb = new Scanner(System.in);
    int x, y, sum;
    char symbol;

    public Calculator(int x, int y, char symbol) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
    }

    public void print() {
        System.out.println("Calculating...");
        Calculating();
    }

    private void Calculating() {
        if (x > 99 || y > 99) {
            System.out.println("Cannot Calculate! ! !");
        } else {
            if (symbol == '+') {
                plus();
            } else if (symbol == '-') {
                minus();
            } else if (symbol == '*') {
                multiply();
            } else if (symbol == '/') {
                divide();
            } else {
                System.out.println("ERROR! ! !");
            }
        }
    }

    private void divide() {
        sum=x/y;
        System.out.println(x + "/" + y + " = " + sum);
    }

    private void multiply() {
        sum=x*y;
        System.out.println(x + "*" + y + " = " + sum);
    }

    private void minus() {
        sum=x-y;
        System.out.println(x + "-" + y + " = " + sum);
    }

    private void plus() {
        sum=x+y;
        System.out.println(x + "+" + y + " = " + sum);
    }

}
